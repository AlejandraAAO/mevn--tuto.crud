//crear rutas para guardar data q vengan del server

const express = require('express');
//crea objeto para definir rutas
const router = express.Router();

const Task = require('../models/task');

router.get('/',async (req, res) => {
    const tasks = await Task.find();
    res.json(tasks);
        
});

//para un solo item
router.get('/:id', async(req,res) => {
    const task = await Task.findById(req.params.id);
    res.json(task);
})




//para q el browser envie datos
router.post('/', async (req,res) => {
    const task = new Task(req.body);
    await task.save();
    res.json({
        status : 'save'
    });   
});
router.put('/:id', async (req, res) => {
    await Task.findByIdAndUpdate(req.params.id, req.body)
   res.json({
       status: 'task update'
   })
});

router.delete('/:id', async (req, res) => {
    await Task.findByIdAndRemove(req.params.id);
    res.json({
        status:'eliminado'
    })

})


module.exports = router;
