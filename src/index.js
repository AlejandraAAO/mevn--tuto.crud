const express = require('express');
const morgan = require('morgan');
const mongoose = require('mongoose');


const app = express();
mongoose.connect('mongodb://localhost/mevn-database')
    .then(db => console.log('db conectada'))
    .catch(err => console.log(err));

//settings express
app.set('port', process.env.PORT || 3000);




//middlewares-funcionan paara procesar url
app.use(morgan('dev'));
app.use(express.json());//ex bodyparser, al recibir info en el bsck de fonmato json, va a tener un objeto

//routes
app.use('api/tasks',require('./routes/tasks'))

//STATICFILES--> ARCHIVOS ENVIADOS AL FRONT
app.use(express.static(__dirname+'/public'));


//server lintening
app.listen(app.get('port'), () => {
    console.log('serve on', app.get('port'));
});
